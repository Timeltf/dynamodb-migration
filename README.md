# Migration between DynamoDB accounts

A lambda function to get all tables and infos from one DynamoDB and replicate it in another account

O script funciona em alguns passos (vamos chamar “origem” o DB que vai ser copiado e “alvo” o DB que vai receber as informações da “origem”):

- Pega todas as informações de todas as tabelas do Dynamo origem
- Exclui todas as informações do Dynamo alvo
- Copia todas as informações do Dynamo de origem para o Dynamo alvo
- Cadastra regras de autoscaling para cada uma das tabelas

Para o script funcionar, tivemos que criar usuários com permissões especificas para o Dynamo nas conta alvo e origem. Além disso na hora de rorar o script algumas informações precisam ser passadas por variáveis de ambiente:

- PROD: identifica se estamos rodando o script em um cenário local, ou se é pra valer mesmo
- PROD_ACCESS_KEY_ID: chave AccessKey da conta origem
- PROD_SECRET_ACCESS_KEY: chave SecretAccessKey da conta origem
- PROD_REGION: região que está o DynamoDB que queremos copiar da conta de origem
- STAGING_ACCESS_KEY_ID: chave AccessKey da conta alvo
- STAGING_SECRET_ACCESS_KEY: chave SecretAccessKey da conta alvo
- STAGING_REGION: região que está o DynamoDB que queremos escrever na conta alvo
- ACCOUNT_ID: numero da conta alvo na AWS
- AUTOSCALING_MAX_CAPACITY: limite superior do autoscaling (qual o máximo de Capacity Units a tabela vai poder ter)
- AUTOSCALING_MIN_CAPACITY: limite inferior do autoscaling (qual o mínimo de Capacity Units a tabela vai poder ter)
- CURRENT_LAMBDA_ROLE: nome da role que dá permissão a criação de regras de autoscaling

