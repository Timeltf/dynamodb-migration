import * as AWS from 'aws-sdk';
import * as _ from 'lodash';

//
// Lambda to migrate all information from dynamodb prod to stagging
//


// The event that it receives is a SNS that fires when the code is deployed
if (process.env.PROD) {
    exports.handler = (event, context, callback) => {
        migrate()
            .then(result => { console.log(result); callback(); })
            .catch(reason => { console.log(reason); callback(reason); });
    };
}

// Development only:
if (!process.env.PROD)
    migrate();

interface ITablesInfoHash {
    [tableName: string]: Array<any>;
}

async function migrate() {

    let sourceDB: AWS.DynamoDB;
    // Prod config
    if (process.env.PROD) {
        AWS.config.update({
            accessKeyId: process.env.PROD_ACCESS_KEY_ID,
            secretAccessKey: process.env.PROD_SECRET_ACCESS_KEY,
            region: process.env.PROD_REGION
        });
        sourceDB = new AWS.DynamoDB();
    } else {
        // DEV only:
        sourceDB = new AWS.DynamoDB({ region: 'sa-east-1', endpoint: 'http://localhost:8000' });
    }


    // Source table info
    const sourceTablesInfoHash: ITablesInfoHash = {};

    // Get all table names and remove error
    const allSourceTables: Array<string> =
        await getAllTablesNames(sourceDB);

    console.log('Get all sourceDB tables. Number of tables: ', allSourceTables.length, 'All tables: ', JSON.stringify(allSourceTables));

    // Populate the hash with the information of each table
    for (let i = 0; i < allSourceTables.length; i++) {
        const tableName: string = allSourceTables[i];
        sourceTablesInfoHash[tableName] = await getAllItemsFromTable(sourceDB, tableName);
        console.log(`Got all information from table ${tableName}, number of data records retrieved: ${sourceTablesInfoHash[tableName].length}`);
        console.log(`${JSON.stringify(sourceTablesInfoHash[tableName])}`);
    }
    console.log('Hash with source information constructed');

    // Transform the hash in a batch request
    const batchRequest: AWS.DynamoDB.BatchWriteItemInput[] = transformObjectsInDynamoBatchRequest(sourceTablesInfoHash);
    console.log('Information divided in batch requests', batchRequest);

    // STAGING config
    let targetDB: AWS.DynamoDB;
    if (process.env.PROD) {
        AWS.config.update({
            accessKeyId: process.env.STAGING_ACCESS_KEY_ID,
            secretAccessKey: process.env.STAGING_SECRET_ACCESS_KEY,
            region: process.env.STAGING_REGION
        });
        targetDB = new AWS.DynamoDB();
    } else {
        // DEV only:
        targetDB = new AWS.DynamoDB({ region: 'sa-east-1', endpoint: 'http://localhost:8000' });
    }


    const allTargetTables: Array<string> = await getAllTablesNames(targetDB);
    console.log('Get all targetDB tables. Number of tables: ', allTargetTables.length);

    // Delete all targetDB tables
    for (let i = 0; i < allTargetTables.length; i++) {
        console.log('Deleting table ', allTargetTables[i]);
        await deleteTable(targetDB, allTargetTables[i]);

        // Everytime I delete a table it takes a while to AWS to process and provide the table and I have to wait till it completes
        await sleep(10000);
        console.log('Table deleted ', allTargetTables[i]);
    }
    console.log('All target tables deleted');

    // Create all tables from sourceDB in targetDB
    for (let i = 0; i < allSourceTables.length; i++) {
        const tableName: string = allSourceTables[i];
        console.log('Creating table ', tableName);

        const tableInfos: AWS.DynamoDB.DescribeTableOutput = await describeTable(sourceDB, tableName);
        console.log(JSON.stringify(tableInfos));
        await createTable(targetDB, tableInfos);

        // Everytime I create a table it takes a while to AWS to process and provide the table and I have to wait till it completes to start putting the information inside it
        await sleep(10000);
    }
    console.log('All target tables created');

    await sleep(120000);

    const autoscalingInstance: AWS.ApplicationAutoScaling = new AWS.ApplicationAutoScaling();

    for (const tableName of allTargetTables) {
        console.log('Creating autoscaling policy for table ', tableName);
        await allowAutoScalingForAllTables(autoscalingInstance, tableName);
        console.log('Autoscaling policy created for table ', tableName);
    }


    console.log('Starting the process of writing all the information in target db');
    await writeAllItems(targetDB, batchRequest);

    console.log('Migration done');
}

/**
 * Handles requests and retry it if we received some unprocessed items
 * @param {AWS.DynamoDB} targetDB
 * @param {AWS.DynamoDB.BatchWriteItemInput[]} allBatchRequest
 * @returns {Promise<void>}
 */
async function writeAllItems(targetDB: AWS.DynamoDB, allBatchRequest: AWS.DynamoDB.BatchWriteItemInput[]): Promise<void> {
    // Try to run all batchWrites
    for (const batchRequest of allBatchRequest) {
        let result: AWS.DynamoDB.Types.BatchWriteItemOutput = await writeBatchItem(targetDB, batchRequest);
        console.log(result);

        // If we have unprocessed items we try to write it again
        let i = 0;
        while (Object.keys(result.UnprocessedItems).length > 0) {
            await sleep(Math.pow(10, i) * 1000);
            const unprocessedInput: AWS.DynamoDB.BatchWriteItemInput = { RequestItems: result.UnprocessedItems };
            console.log('--> unprocessed', unprocessedInput);
            result = await writeBatchItem(targetDB, unprocessedInput);
            i++;
        }
    }
}

/**
 * Just transform writeBatchItem in a Promise based approach
 * @param {AWS.DynamoDB} targetDB
 * @param {AWS.DynamoDB.BatchWriteItemInput} input
 * @returns {Promise<AWS.DynamoDB.Types.BatchWriteItemOutput>}
 */
async function writeBatchItem(targetDB: AWS.DynamoDB, input: AWS.DynamoDB.BatchWriteItemInput): Promise<AWS.DynamoDB.Types.BatchWriteItemOutput> {
    return new Promise<AWS.DynamoDB.Types.BatchWriteItemOutput>((resolve, reject) => {
        targetDB.batchWriteItem(input, (err, data: AWS.DynamoDB.Types.BatchWriteItemOutput) => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

/**
 * Create table in target DB receiving as parameter the output from describeTable from sourceDB
 * @param {AWS.DynamoDB} db
 * @param {AWS.DynamoDB.DescribeTableOutput} describeTable
 * @returns
 */
async function createTable(targetDB: AWS.DynamoDB, describeTable: AWS.DynamoDB.DescribeTableOutput): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        const input: AWS.DynamoDB.Types.CreateTableInput = {
            AttributeDefinitions: describeTable.Table.AttributeDefinitions,
            TableName: describeTable.Table.TableName,
            KeySchema: describeTable.Table.KeySchema,
            ProvisionedThroughput: {
                ReadCapacityUnits: parseInt(process.env.AUTOSCALING_MIN_CAPACITY),
                WriteCapacityUnits: parseInt(process.env.AUTOSCALING_MIN_CAPACITY)
            }
        };
        // Mount global secondary index
        if (describeTable.Table.GlobalSecondaryIndexes) {
            input.GlobalSecondaryIndexes = describeTable.Table.GlobalSecondaryIndexes
                .map((glSecIndex: AWS.DynamoDB.GlobalSecondaryIndexDescription, i: number) => {
                    const index: AWS.DynamoDB.GlobalSecondaryIndex = {
                        IndexName: glSecIndex.IndexName,
                        KeySchema: glSecIndex.KeySchema,
                        Projection: glSecIndex.Projection,
                        ProvisionedThroughput: {
                            ReadCapacityUnits: parseInt(process.env.AUTOSCALING_MIN_CAPACITY),
                            WriteCapacityUnits: parseInt(process.env.AUTOSCALING_MIN_CAPACITY),
                        }
                    };
                    return index;
                });
        }
        targetDB.createTable(input, (err, data: AWS.DynamoDB.CreateTableOutput) => {
            if (err) reject(err);
            resolve(data);
        });
    });
}

/**
 * Delete table
 * @param {AWS.DynamoDB} targetDB
 * @param {string} tableName
 * @returns
 */
async function deleteTable(targetDB: AWS.DynamoDB, tableName: string) {
    return new Promise((resolve, reject) => {
        const input: AWS.DynamoDB.Types.DeleteTableInput = { TableName: tableName };
        targetDB.deleteTable(input, (err, data: AWS.DynamoDB.Types.DeleteTableOutput) => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

async function describeTable(db: AWS.DynamoDB, tableName: string): Promise<AWS.DynamoDB.DescribeTableOutput> {
    return new Promise<AWS.DynamoDB.DescribeTableOutput>((resolve, reject) => {
        const input: AWS.DynamoDB.DescribeTableInput = { TableName: tableName };
        db.describeTable(input, (err, data: AWS.DynamoDB.DescribeTableOutput) => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

/**
 * Get the info hash and transform it in a request to batchWriteItem
 * I have to break the batch request in only 25 items
 * @param {ITablesInfoHash} sourceHashTableInfo
 * @returns {AWS.DynamoDB.BatchWriteItemInput}
 */
function transformObjectsInDynamoBatchRequest(sourceHashTableInfo: ITablesInfoHash): AWS.DynamoDB.BatchWriteItemInput[] {
    const arrayWithBatchItems: AWS.DynamoDB.BatchWriteItemInput[] = [];
    let index: number = -1;

    // Run through all tables
    Object.keys(sourceHashTableInfo)
        .filter((tableName: string) => sourceHashTableInfo[tableName].length > 0)
        .map((tableName: string) => {

            // Everytime I change tableName I have to create another object of batchWrites
            arrayWithBatchItems.push({ RequestItems: {} });
            index++;
            arrayWithBatchItems[index].RequestItems[tableName] = [];

            // Map the object to a write request
            const allWriteRequestsForTheTable: AWS.DynamoDB.Types.WriteRequest[] = sourceHashTableInfo[tableName]
                .map((object: any) => {
                    const writeRequest: AWS.DynamoDB.Types.WriteRequest = {
                        PutRequest: {
                            Item: object
                        }
                    };
                    return writeRequest;
                });


            // Now I have all the write requests and I need to check if I have more then 25 write requests,
            // if so I have to make chunks of data to be batched

            // Run through all writeRequests
            allWriteRequestsForTheTable.map((writeRequest: AWS.DynamoDB.Types.WriteRequest, i: number) => {
                arrayWithBatchItems[index].RequestItems[tableName].push(writeRequest);

                // If we get to 25 items in an array we must create another object
                if (i % 24 == 0) {
                    arrayWithBatchItems.push({ RequestItems: {} });
                    index++;
                    arrayWithBatchItems[index].RequestItems[tableName] = [];
                }
            });
        });

    // Erase all the requests that does not have nothing inside
    const filtered1 = arrayWithBatchItems
        .map((value: AWS.DynamoDB.BatchWriteItemInput) => {
            Object.keys(value.RequestItems).map((tableName: string) => {
                if (value.RequestItems[tableName].length <= 0)
                    delete value.RequestItems[tableName];
            });

            return value;
        });
    const filtered2 = filtered1.filter((value: AWS.DynamoDB.BatchWriteItemInput) => {
        return !_.isEmpty(value.RequestItems) && Object.keys(value.RequestItems).length > 0;
    });

    return filtered2;
}

async function getAllTablesNames(db: AWS.DynamoDB): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        db.listTables((err, data: AWS.DynamoDB.ListTablesOutput) => {
            if (err) reject(err);
            else resolve(data.TableNames);
        });
    });
}

async function getAllItemsFromTable<T>(sourceDB: AWS.DynamoDB, tableName: string): Promise<Array<any>> {
    const input: AWS.DynamoDB.ScanInput = {
        TableName: tableName,
        ConsistentRead: true
    };
    return new Promise<Array<any>>((resolve, reject) => {
        // Call DynamoDB to retrieve the list of tables
        sourceDB.scan(input, (err: AWS.AWSError, data: AWS.DynamoDB.ScanOutput) => {
            if (err) reject(err);
            else resolve(data.Items);
        });
    });
}

async function sleep(ms): Promise<any> {
    return new Promise<any>(resolve => setTimeout(resolve, ms));
}


/**
 * Create an autoscaling process for a table
 *
 * @param {AWS.ApplicationAutoScaling} autoscalingInstance
 * @param {string} tableName
 */
async function allowAutoScalingForAllTables(autoscalingInstance: AWS.ApplicationAutoScaling, tableName: string) {

    //
    // Auto scaling write capacity
    //
    const autoscalingWriteParams: AWS.ApplicationAutoScaling.Types.RegisterScalableTargetRequest = {
        MaxCapacity: parseInt(process.env.AUTOSCALING_MAX_CAPACITY),
        MinCapacity: parseInt(process.env.AUTOSCALING_MIN_CAPACITY),
        ResourceId: "table/" + tableName,
        RoleARN: "arn:aws:iam::" + process.env.ACCOUNT_ID + `:role/${process.env.CURRENT_LAMBDA_ROLE}`,
        ScalableDimension: "dynamodb:table:WriteCapacityUnits",
        ServiceNamespace: "dynamodb"
    };

    const autoscalingWriteRegisterResponse: AWS.ApplicationAutoScaling.Types.RegisterScalableTargetResponse = await awsCallAsync<AWS.ApplicationAutoScaling.Types.RegisterScalableTargetRequest, AWS.ApplicationAutoScaling.Types.RegisterScalableTargetResponse>(autoscalingInstance, autoscalingInstance.registerScalableTarget, autoscalingWriteParams);

    //
    // Auto scaling write scaling policy
    //

    const scalingWritePolicy: AWS.ApplicationAutoScaling.Types.PutScalingPolicyRequest = {
        ServiceNamespace: "dynamodb",
        ResourceId: "table/" + tableName,
        ScalableDimension: "dynamodb:table:WriteCapacityUnits",
        PolicyName: tableName + "-scaling-policy",
        PolicyType: "TargetTrackingScaling",
        TargetTrackingScalingPolicyConfiguration: {
            PredefinedMetricSpecification: {
                PredefinedMetricType: "DynamoDBWriteCapacityUtilization"
            },
            ScaleOutCooldown: 60,
            ScaleInCooldown: 60,
            TargetValue: 70.0
        }
    };

    const scalingWritePolicyResponse: AWS.ApplicationAutoScaling.Types.PutScalingPolicyResponse = await awsCallAsync<AWS.ApplicationAutoScaling.Types.PutScalingPolicyRequest, AWS.ApplicationAutoScaling.Types.PutScalingPolicyResponse>(autoscalingInstance, autoscalingInstance.putScalingPolicy, scalingWritePolicy);


    //
    // Auto scaling read capacity
    //
    const autoscalingReadParams: AWS.ApplicationAutoScaling.Types.RegisterScalableTargetRequest = {
        MaxCapacity: parseInt(process.env.AUTOSCALING_MAX_CAPACITY),
        MinCapacity: parseInt(process.env.AUTOSCALING_MIN_CAPACITY),
        ResourceId: "table/" + tableName,
        RoleARN: "arn:aws:iam::" + process.env.ACCOUNT_ID + `:role/${process.env.CURRENT_LAMBDA_ROLE}`,
        ScalableDimension: "dynamodb:table:ReadCapacityUnits",
        ServiceNamespace: "dynamodb"
    };

    const autoscalingReadRegisterResponse: AWS.ApplicationAutoScaling.Types.RegisterScalableTargetResponse = await awsCallAsync<AWS.ApplicationAutoScaling.Types.RegisterScalableTargetRequest, AWS.ApplicationAutoScaling.Types.RegisterScalableTargetResponse>(autoscalingInstance, autoscalingInstance.registerScalableTarget, autoscalingReadParams);

    //
    // Auto scaling read scaling policy
    //

    const scalingReadPolicy: AWS.ApplicationAutoScaling.Types.PutScalingPolicyRequest = {
        ServiceNamespace: "dynamodb",
        ResourceId: "table/" + tableName,
        ScalableDimension: "dynamodb:table:ReadCapacityUnits",
        PolicyName: tableName + "-scaling-policy",
        PolicyType: "TargetTrackingScaling",
        TargetTrackingScalingPolicyConfiguration: {
            PredefinedMetricSpecification: {
                PredefinedMetricType: "DynamoDBReadCapacityUtilization"
            },
            ScaleOutCooldown: 60,
            ScaleInCooldown: 60,
            TargetValue: 70.0
        }
    };

    const scalingReadPolicyResponse: AWS.ApplicationAutoScaling.Types.PutScalingPolicyResponse = await awsCallAsync<AWS.ApplicationAutoScaling.Types.PutScalingPolicyRequest, AWS.ApplicationAutoScaling.Types.PutScalingPolicyResponse>(autoscalingInstance, autoscalingInstance.putScalingPolicy, scalingReadPolicy);
}

type TAWSFunction<T, U> = (params: T, callback?: (err: AWS.AWSError, data: U) => void) => AWS.Request<U, AWS.AWSError>;

async function awsCallAsync<T, U>(awsInstance: AWS.DynamoDB | AWS.ApplicationAutoScaling, inputFunction: TAWSFunction<T, U>, params: T): Promise<U> {

    // I have to bind the object in order to make the callback work
    inputFunction = inputFunction.bind(awsInstance);

    return new Promise<U>((resolve, reject) => {
        inputFunction(params, (err: AWS.AWSError, data: U) => {
            if (_.isEmpty(data)) resolve(null);
            if (err) reject(err);
            else resolve(data);
        });
    });
}
